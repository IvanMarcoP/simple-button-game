using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ContadorController : MonoBehaviour
{
    public TextMeshProUGUI contadorTxt;
    public int contador = 0;

    // Start is called before the first frame update
    void Start()
    {
        actualizarContador();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void incrementarContador(int cantidad)
    {
        contador += cantidad;
        actualizarContador();
    }

    void actualizarContador()
    {
        contadorTxt.text = contador.ToString();
    }
}
