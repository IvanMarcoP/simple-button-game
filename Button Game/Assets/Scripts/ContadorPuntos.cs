using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class ContadorPuntos : MonoBehaviour
{
    public TextMeshProUGUI TxtTexto;
    public int contador = 0;

    // Start is called before the first frame update
    void Start()
    {
        ActualizarTextos();
    }

    public void conteoPuntos()
    {
        contador++;
        ActualizarTextos();
    }

    void ActualizarTextos()
    {
        TxtTexto.text = contador.ToString();
    }
}
