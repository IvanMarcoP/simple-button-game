using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Bala_Movement : MonoBehaviour
{
    public float velocidad = 7f;

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.up * velocidad * Time.deltaTime);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }
}
