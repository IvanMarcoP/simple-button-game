using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LifeController : MonoBehaviour
{
    public TextMeshProUGUI vidasTxt;
    public int vidaMaxima = 3;
    public int vidasActuales;
    public GameManager GM;

    // Start is called before the first frame update
    void Start()
    {
        vidasActuales = vidaMaxima;
        ActualizarTexto();
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Enemy"))
        {
            PerderVida();
            Destroy(other.gameObject);
        }
    }

    void PerderVida()
    {
        vidasActuales--;
        ActualizarTexto();

        if(vidasActuales <= 0)
        {
            GM.gameover = true;
            Time.timeScale = 0f;
        }
    }

    void ActualizarTexto()
    {
        vidasTxt.text = vidasActuales.ToString();
    }
}
