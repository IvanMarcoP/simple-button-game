using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Controlador_Contador : MonoBehaviour
{
    [SerializeField] private Camera camara;
    public Touch miToque;
    public TextMeshProUGUI touchCountText; 
    //private int touchCount = 0;
    public int contador = 0;

    void Start()
    {
        camara = Camera.main;
        ActualizarTexto();
    }

    void Update()
    {
        // Verificar si hay al menos un toque en la pantalla
        if (Input.touchCount > 0)
        {
            miToque = Input.GetTouch(0);
            Ray ray = Camera.main.ScreenPointToRay(miToque.position);
            RaycastHit hit;

            if(Physics.Raycast(ray, out hit))
            {
                contador++;
                ActualizarTexto();
            }
        }
    }

    void ActualizarTexto()
    {
        touchCountText.text = contador.ToString();
    }
}
