using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner_Controller : MonoBehaviour
{
    public GameObject enemy;
    public Transform[] posiciones;
    public float intervaloTiempo = 2f;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnObject", 0f, intervaloTiempo);   
    }

    void SpawnObject()
    {
        int rndPosiciones = Random.Range(0, posiciones.Length);

        Transform spawnPosiciones = posiciones[rndPosiciones];

        Instantiate(enemy, spawnPosiciones.position, Quaternion.identity);
    }
}
