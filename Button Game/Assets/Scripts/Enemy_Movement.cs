using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Movement : MonoBehaviour
{
    public float speed = 5f;
    public int puntosGanados = 1;
    public ContadorController CC;

    // Start is called before the first frame update
    void Start()
    {
        CC = GameObject.Find("ContadorController").GetComponent<ContadorController>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.down * speed * Time.deltaTime);
    }

    void OnDestroy()
    {
        CC.incrementarContador(puntosGanados);
    }
}
