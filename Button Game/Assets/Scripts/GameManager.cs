using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public TextMeshProUGUI gameOverTxt;
    public Button restart;
    public bool gameover;

    // Start is called before the first frame update
    void Start()
    {
        gameover = false;
        gameOverTxt.enabled = false;
        restart.gameObject.SetActive(false);

        Time.timeScale = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        if(gameover)
        {
            gameOverTxt.enabled = true;
            restart.gameObject.SetActive(true);
        }
    }

    public void restartScene()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;

        SceneManager.LoadScene(currentSceneIndex);
    }
}
