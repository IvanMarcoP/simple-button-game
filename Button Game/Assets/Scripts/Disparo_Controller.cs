using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparo_Controller : MonoBehaviour
{
    [SerializeField] private Camera camara;
    public GameObject bala;
    public Transform posicion;

    void Start()
    {
        camara = Camera.main;
    }

    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch miToque = Input.GetTouch(0);
            Ray ray = Camera.main.ScreenPointToRay(miToque.position);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                //posicion.position = hit.point;
                //Disparar();
                if (hit.collider.CompareTag("Enemy"))
                {
                    Destroy(hit.collider.gameObject);
                }
            }
        }
    }

    public void Disparar()
    {
        Instantiate(bala, posicion.position, posicion.rotation);
    }
}
